<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from dreamguys.co.in/demo/doccure/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:07:10 GMT -->
<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Doccure - Dashboard</title>

		<!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

		<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ asset('backend/css/bootstrap.min.css') }}">

		<!-- Fontawesome CSS -->
        <link rel="stylesheet" href="{{ asset('backend/css/font-awesome.min.css') }}">

		<!-- Feathericon CSS -->
        <link rel="stylesheet" href="{{ asset('backend/css/feathericon.min.css') }}">

		<link rel="stylesheet" href="{{ asset('backend/plugins/morris/morris.css') }}">

        @stack('style')

		<!-- Main CSS -->
        <link rel="stylesheet" href="{{ asset('backend/css/style.css') }}">



		<!--[if lt IE 9]>
			<script src="backend/js/html5shiv.min.js"></script>
			<script src="backend/js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>

		<!-- Main Wrapper -->
        <div class="main-wrapper">


            @include('backend.partials.header')


            @include('backend.partials.sidebar')


            @yield('content')


        </div>
		<!-- /Main Wrapper -->









		<!-- jQuery -->
        <script src="{{ asset('backend/js/jquery-3.2.1.min.js') }}"></script>

		<!-- Bootstrap Core JS -->
        <script src="{{ asset('backend/js/popper.min.js') }}"></script>
        <script src="{{ asset('backend/js/bootstrap.min.js') }}"></script>

		<!-- Slimscroll JS -->
        <script src="{{ asset('backend/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

		<script src="{{ asset('backend/plugins/raphael/raphael.min.js') }}"></script>
		<script src="{{ Asset('backend/plugins/morris/morris.min.js') }}"></script>
		<script src="{{ Asset('backend/js/chart.morris.js') }}"></script>


        @stack('scripts')


		<!-- Custom JS -->
        <script  src="{{ asset('backend/js/script.js') }}"></script>


    </body>

</html>
