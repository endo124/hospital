<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from dreamguys.co.in/demo/doccure/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 24 Dec 2019 21:06:25 GMT -->
<head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
		<title>Doccure</title>

		<!-- Favicons -->
		<link type="image/x-icon" href="{{asset('frontend/img/favicon.png')}}" rel="icon">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">

		<!-- Fontawesome CSS -->
		<link rel="stylesheet" href="{{asset('frontend/plugins/fontawesome/css/fontawesome.min.css')}}">
		<link rel="stylesheet" href="{{asset('frontend/plugins/fontawesome/css/all.min.css')}}">

		<!-- Main CSS -->
		<link rel="stylesheet" href="{{asset('frontend/css/style.css')}}">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="{{ asset('frontend/js/html5shiv.min.js') }}"></script>
			<script src="{{ asset('frontend/js/respond.min.js') }}"></script>
        <![endif]-->

        @stack('style')

	</head>
	<body>


    <!-- Main Wrapper -->
		<div class="main-wrapper">

            @include('frontend.partials.header')



            @yield('content')




            @include('frontend.partials.footer')


	   </div>
	<!-- /Main Wrapper -->

        <!-- jQuery -->
		<script src="{{ asset('frontend/js/jquery.min.js') }}"></script>

		<!-- Bootstrap Core JS -->
		<script src="{{ asset('frontend/js/popper.min.js') }}"></script>
		<script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>

		<!-- Slick JS -->
		<script src="{{ asset('frontend/js/slick.js') }}"></script>

		<!-- Custom JS -->
        <script src="{{ asset('frontend/js/script.js') }}"></script>


        <script type="text/javascript">
            if (window.location.hash && window.location.hash == '#_=_') {
                window.location.hash = '';
            }
        </script>
        @stack('scripts')

    </body>


</html>
