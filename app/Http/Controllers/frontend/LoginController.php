<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Models\User;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function index()
    {

       return view('frontend.login');
    }

    public function checkLogin(Request $request)
    {

        request()->validate([
        'email' => 'required',
        'password' => 'required',
        ]);

        $user=User::where('email',$request->email)->first();

            $credentials = $request->only('email', 'password');
            if (Auth::attempt(['email' => $credentials['email'], 'password' => $credentials['password']]) && $user['email_verified_at'] && $user->roles[0]->name == 'paient') {

                Auth::login($user);
                return redirect()->to('frontend/Home');
            }elseif(Auth::attempt(['email' => $credentials['email'], 'password' => $credentials['password']]) && $user['email_verified_at'] && $user->roles[0]->name == 'admin' ){
                return redirect()->to('backend/login');
            }else{
                return Redirect::to("frontend/login")->withSuccess('Oppes! You have entered invalid credentials');

            }


    }


    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->to('frontend/Home');
    }



    public function verify($id){

        $user=User::find($id);

        $user->email_verified_at=Carbon::now();
        $user->save();
        return redirect('frontend/login');

    }

}
