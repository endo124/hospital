<?php

namespace App\Http\Controllers\frontend;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
class SocialController extends Controller
{

    public function redirect($provider)
    {
     return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider){
        $userSocial =   Socialite::driver($provider)->stateless()->user();
        $users       =   User::where('email' , $userSocial->getEmail())->first();
    if(!empty($users) && $users->roles[0]->name == 'patient'){

                Auth::login($users);
            }
            elseif(empty($users)){
    $user = User::create([
                    'name'          => $userSocial->getName(),
                    'email'         => $userSocial->getEmail(),
                    'provider_id'   => $userSocial->getId(),
                    'provider'      => $provider,
                    'email_verified_at'=>Carbon::now(),
                ]);
                $user->roles()->attach(3);

                Auth::login($user);

        }
        elseif(!empty($users) &&$users->roles[0]->name == 'admin' ){
            return redirect('/backend/login');
        }
        return redirect('/frontend/Home');

    }
}
