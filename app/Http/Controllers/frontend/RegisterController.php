<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\VerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function index()
    {
        return view('frontend.registeration');

    }

    public function store(Request $request)
    {
        $validate=$request->validate([
            'name'=>'required',
            'email'=>'required|unique:users',
            'phone'=>'required|regex:/(01)[0-9]{9}/|unique:users',
            'password'=>'required'
        ]);
        $user=$request->except($request->password);
        $password=Hash::make($request->password);
        $user['password']=$password;
        $user=User::create($user);
        $user->notify((new VerifyEmail($user)));

        $user->roles()->attach(3);

        return 'check your email for verifiaction';


    }
}
