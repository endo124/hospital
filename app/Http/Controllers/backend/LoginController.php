<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{

    public function index()
    {
        if(auth()->check()){
            return view('backend.index');

        }else{
            return redirect('backend/login');
        }
    }

    public function login()
    {
        return view('backend.login');
    }


    public function checkLogin(Request $request)
    {

        $validate=$request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);

        $admin=User::where('email',$request->email)->with('roles')->first();

        $credentials=$request->only(['email','password']);


        if(Auth::attempt($credentials) && $admin->roles[0]->name =='admin'){

            Auth::login($admin);
            return redirect()->to('backend/Home');
        }
        return Redirect::to("backend/login")->withSuccess('Oppes! You have entered invalid credentials');


    }

    public function logout(Request $request)
    {
        Auth::logout();
        return Redirect::to("backend/login");
    }
}
