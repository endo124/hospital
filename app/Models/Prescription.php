<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    
    protected $fillable = [
         'prescription'
    ];




    public function schedule(){
        return $this->hasOne('App\Models\Schedule','schedule_id');
    }

    public function users(){
        return $this->belongsToMany('App\Models\User','prescription_id','user_id');
    }
}
