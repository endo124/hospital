<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable = [
        'name'
    ];

    /*start many to many relation*/

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission','permissions_roles','permission_id','role_id');
    }

    /*end many to many relation*/
}
