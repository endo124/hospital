<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_Details extends Model
{
    protected $fillable = [
        'schedule_id','about_me','education','experience','awards','pricing'
    ];


    /*start one to many relation*/

    public function schedules()
    {
        return $this->belongsTo('App\Models\Schedules','schedule_id');
    }

    /*start one to many relation*/




    /*one to one relation */

    public function user()
    {
        return $this->hasOne('App\Models\User','user_id');
    }
}
