<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'date','from','to'
    ];


    /*one to one relation */

    public function prescription(){
        return $this->hasOne('App\Models\Prescription','prescription_id');
    }

    /*end to one relation */




    
    /*start one to many relation*/

    public function user_details(){
        return $this->hasMany('App\Models\User_Details');
    }

    /*end one to many relation*/


}
