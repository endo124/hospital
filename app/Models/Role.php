<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name'
    ];


    /*start many to many relation*/

    public function permissions()
    {
        return $this->belongsToMany('App\Models\Permission','permissions_roles','role_id','permission_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User','roles_users','role_id','user_id');
    }

    /*end many to many relation*/
}
