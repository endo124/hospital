<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public $fillable=['name','src_image'];



    public function users(){
        return $this->belongsToMany('App\Models\User','department_id','user_id');
    }

}
