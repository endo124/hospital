<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','provider','provider_id','address','phone','gender','blood_group','national_number','src_image','date_of_birth','email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /*start many to many relation*/

    public function departments(){
        return $this->belongsToMany('App\Models\Department','departments_users','user_id','department_id');
    }

    public function prescriptions(){
        return $this->belongsToMany('App\Models\Prescription','prescriptions_users','user_id','prescription_id');
    }

    public function roles(){
        return $this->belongsToMany('App\Models\Role','roles_users','user_id','role_id');
    }

    /*end many to many relation*/



    /*one to one relation */
    public function user_details(){
        return $this->belongsTo('App\Models\User_Details','user_id');
    }

}
