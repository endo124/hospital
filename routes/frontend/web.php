<?php

use Illuminate\Support\Facades\Route;

Route::get('login/{provider}', 'frontend\SocialController@redirect');
Route::get('login/{provider}/callback','frontend\SocialController@Callback');




Route::group(['prefix' => 'frontend','namespace'=>'frontend'], function () {

    Route::get('/Home',function(){

        return view('frontend.index');
    });





    Route::get('/login','LoginController@index');
    Route::post('/login','LoginController@checkLogin');

    Route::get('/register','RegisterController@index');
    Route::post('/register','RegisterController@store');

    Route::post('/logout','LoginController@logout');

    Route::get('verify/{id}','LoginController@verify');







});
